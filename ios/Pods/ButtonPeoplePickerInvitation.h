//
//  ButtonPeoplePickerInvitation.h
//  SmallGroup
//
//  Created by Austin Aldrich on 10/12/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AddressBook/AddressBook.h>

@interface ButtonPeoplePickerInvitation : NSObject

@property (nonatomic, assign) ABRecordRef record;
@property (nonatomic, strong) NSString *inviteMethod;
@property (nonatomic, strong) NSString *inviteValue;


@end
