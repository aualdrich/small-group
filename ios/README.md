#Small Group

Small Group is an app designed to help church small groups live out their lives of faith together throughout the week through:
* Prayer Requests
* Group Chat
* Events

##Setup

In your info.plst, set two keys:

parse_application_id
parse_client_key

You can get these from the Parse site.


##Screenshots

![](http://i1301.photobucket.com/albums/ag102/aualdrich/menu_zpseav8ypi2.png "Menu")
![](http://i1301.photobucket.com/albums/ag102/aualdrich/event_zpsmr99oldc.png "Events")
![](http://i1301.photobucket.com/albums/ag102/aualdrich/chat_zpsrc9xlesd.png "Chat")
![](http://i1301.photobucket.com/albums/ag102/aualdrich/prayer_request_zps7qaa0fnf.png "Prayer Request")