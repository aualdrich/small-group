//
//  ChatViewController.swift
//  SmallGroup
//
//  Created by Austin Aldrich on 1/3/15.
//  Copyright (c) 2015 Missions in Motion. All rights reserved.
//

import Foundation

class ChatViewController : JSQMessagesViewController {
    
    var messages = [ChatMessage]()
    var bubbleImageFactory = JSQMessagesBubbleImageFactory()
    var outgoingBubbleImage :JSQMessagesBubbleImage?
    var incomingBubbleImage :JSQMessagesBubbleImage?
    var query = PFQuery(className: "ChatMessage")
    var pageSize = 15
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        query.whereKey("group", equalTo: PFUser.currentUser().getActiveGroup())
        query.orderByDescending("createdAt")
        query.limit = self.pageSize
        
        self.senderId = PFUser.currentUser().objectId
        self.senderDisplayName = PFUser.currentUser().getFullName()
        
        self.outgoingBubbleImage = bubbleImageFactory.outgoingMessagesBubbleImageWithColor(UIColor.jsq_messageBubbleLightGrayColor())
        
        self.incomingBubbleImage = bubbleImageFactory.incomingMessagesBubbleImageWithColor(UIColor.jsq_messageBubbleBlueColor())
        
        self.showLoadEarlierMessagesHeader = true
        
       
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.getLatestMessages()
        
    }
    
    func getLatestMessages() {
        
        query.limit = self.messages.count + self.pageSize
        
        
        var objects = query.findObjects()
        
        var tmpMessages = objects as [ChatMessage]
        self.messages = tmpMessages
        self.messages = self.messages.reverse()
        
        self.finishReceivingMessageAnimated(false)
            
        
    }
    
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.messages.count
    }
    
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, messageDataForItemAtIndexPath indexPath: NSIndexPath!) -> JSQMessageData! {
        var message = self.messages[indexPath.row]
        return message
        
    }

    
    override func collectionView(collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAtIndexPath indexPath: NSIndexPath!) -> JSQMessageBubbleImageDataSource! {
        
        var message = self.messages[indexPath.row]
        
        if(message.senderId() == self.senderId) {
            return self.outgoingBubbleImage
        }
        
        return self.incomingBubbleImage
        
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAtIndexPath indexPath: NSIndexPath!) -> JSQMessageAvatarImageDataSource! {
        
        var backgroundColor = Branding.getDarkestGreyColor()
        var textColor = UIColor.whiteColor()
        
        var message = self.messages[indexPath.row]
        
        var user = message.objectForKey("sender") as PFUser
        user.fetchIfNeeded()
        
        var avatarImage = JSQMessagesAvatarImageFactory.avatarImageWithUserInitials(user.getInitials(), backgroundColor: backgroundColor, textColor: textColor, font: UIFont.systemFontOfSize(14.0), diameter: 30)
        
       
        return avatarImage
        
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, attributedTextForMessageBubbleTopLabelAtIndexPath indexPath: NSIndexPath!) -> NSAttributedString! {
        var message = self.messages[indexPath.row]
        
        return NSAttributedString(string: message.senderDisplayName())
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, attributedTextForCellBottomLabelAtIndexPath indexPath: NSIndexPath!) -> NSAttributedString! {
        return nil
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, attributedTextForCellTopLabelAtIndexPath indexPath: NSIndexPath!) -> NSAttributedString! {
        
        return nil
        
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellTopLabelAtIndexPath indexPath: NSIndexPath!) -> CGFloat {
        
        return 20
        
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAtIndexPath indexPath: NSIndexPath!) -> CGFloat {
        
        
//        var currentMessage = self.messages[indexPath.item]
//        
//        if(currentMessage.senderId() == self.senderId) {
//            return 0
//        }
//        
//        if(indexPath.item - 1 > 0) {
//            var previousMessage = self.messages[indexPath.item - 1]
//            if(previousMessage.senderId() == currentMessage.senderId()) {
//                return 0
//            }
//        }
        
        return 20
        
        
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellBottomLabelAtIndexPath indexPath: NSIndexPath!) -> CGFloat {
        return 0
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        var cell = super.collectionView(collectionView, cellForItemAtIndexPath: indexPath) as JSQMessagesCollectionViewCell
        
        var message = self.messages[indexPath.item]
        
        if(message.senderId() == self.senderId) {
            cell.textView.textColor = UIColor.blackColor()
        }
        else {
            cell.textView.textColor = UIColor.whiteColor()
        }
        
        return cell
        
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, header headerView: JSQMessagesLoadEarlierHeaderView!, didTapLoadEarlierMessagesButton sender: UIButton!) {
        
        
        self.getLatestMessages()
        
        
    }
    
    
    override func didPressSendButton(button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: NSDate!) {
        
        JSQSystemSoundPlayer.jsq_playMessageSentSound()
        
        var message = ChatMessage()
        
        message.setObject(PFUser.currentUser(), forKey: "sender")
        message["text"] = text
        
        var activeGroup = PFUser.currentUser().objectForKey("active_group") as PFObject
        
        message.setObject(activeGroup, forKey: "group")
        message.save()
        
        var channelName = PushChannelFormatters.getGroupChannelName(activeGroup.objectId)
        
        PFPush.sendPushDataToChannel(channelName, withData: ["alert": text,  "push_type": PushTypes.GroupChat.rawValue, "message_id": message.objectId], error: nil)
        
        self.finishSendingMessageAnimated(true)
        
    }
    
    
    
    
}