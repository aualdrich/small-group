//
//  GroupResultViewController.swift
//  SmallGroup
//
//  Created by Austin Aldrich on 12/24/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

import Foundation

class GroupResultViewController : UIViewController {
    
    var group :PFObject?
    @IBOutlet var groupNameLabel :UILabel?
    @IBOutlet var groupSubtitleLabel :UILabel?
    @IBOutlet var groupPurposeTextView :UITextView?
    
    override func viewDidLoad() {
        
        self.navigationItem.title = group!["name"] as? String
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Join Group", style: UIBarButtonItemStyle.Done, target: self, action: "joinGroup")
        
        self.groupNameLabel?.text = group!["name"] as? String
        
        var location = group!["location"] as? String
        var organization = group!["org_name"] as? String
        
        if(location == nil) { location = "?" }
        if(organization == nil) { organization = "" }
        
        var subTitle = "\(organization!) - \(location!)"
        
        self.groupSubtitleLabel?.text = subTitle
        
        self.groupPurposeTextView?.text = group!["purpose"] as? String
        
    }
    
    @IBAction func joinGroup() {
        
        var existingRequestQuery = PFQuery(className: "GroupRequest")
        existingRequestQuery.whereKey("requester", equalTo: PFUser.currentUser())
        existingRequestQuery.whereKey("group", equalTo: self.group)
        
        var results = existingRequestQuery.findObjects() as? [PFObject]
        
        if(results != nil) {
        
            var request = PFObject(className: "GroupRequest")
            request.setObject(PFUser.currentUser(), forKey: "requester")
            request.setObject(self.group!, forKey: "group")
            request["approved"] = false
            
            request.saveInBackgroundWithBlock({ (result :Bool, error :NSError!) -> Void in
                self.view.makeToast("Request sent!", duration: 3.0, position: "center")
            })
            
        }
    }
    
    
    
}