//
//  PushNotificationEnums.swift
//  SmallGroup
//
//  Created by Austin Aldrich on 1/11/15.
//  Copyright (c) 2015 Missions in Motion. All rights reserved.
//

import Foundation

enum PushTypes : String {
    case GroupChat = "group_chat"
}
