//
//  MemberRequestsViewController.swift
//  SmallGroup
//
//  Created by Austin Aldrich on 12/25/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

import Foundation

class MemberRequestsViewController : UITableViewController {
    
    
    var requests = [PFObject]()
   
    
    override func viewWillAppear(animated: Bool) {
        loadData()
    }
    
    
    func loadData() {
        var query = PFQuery(className: "GroupRequest")
        query.whereKey("group", equalTo: PFUser.currentUser().objectForKey("active_group"))
        
        self.requests = query.findObjects() as [PFObject]
        
        self.tableView.reloadData()
        
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.requests.count
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCellWithIdentifier("cell") as? UITableViewCell
        
        if(cell == nil) {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
            
            var approveButton = UIButton(frame: CGRectMake(40, 0, 70, 30))
            approveButton.setTitle("Approve", forState: UIControlState.Normal)
            approveButton.setTitleColor(UIColor.greenColor(), forState: UIControlState.Normal)
            approveButton.addTarget(self, action: "approveClicked:", forControlEvents: UIControlEvents.TouchUpInside)
            approveButton.tag = indexPath.row
            
            var denyButton = UIButton(frame: CGRectMake(120, 0, 70, 30))
            denyButton.setTitle("Deny", forState: UIControlState.Normal)
            denyButton.setTitleColor(UIColor.redColor(), forState: UIControlState.Normal)
            denyButton.addTarget(self, action: "denyClicked:", forControlEvents: UIControlEvents.TouchUpInside)
            denyButton.tag = indexPath.row
            
            var buttonsView = UIView(frame: CGRectMake(0, 0, 200, 30))
            buttonsView.addSubview(approveButton)
            buttonsView.addSubview(denyButton)
            
            
            cell?.accessoryView = buttonsView
            
            
        }
        
        var request = self.requests[indexPath.row]
        var requester = request.objectForKey("requester") as PFObject
        requester.fetchIfNeeded()
        
        var firstName = requester["first_name"] as? String
        if(firstName == nil) { firstName = "?" }
        
        var lastName = requester["last_name"] as? String
        if(lastName == nil) { lastName = "?" }
        
        var name = "\(firstName!) \(lastName!)"
        
        cell?.textLabel?.text = name
        
        return cell!
        
    }
    
    
    @IBAction func approveClicked(sender :AnyObject!) {
        var request = self.requests[sender.tag]
        
        var requester = request.objectForKey("requester") as PFUser
        
        var activeGroup = PFUser.currentUser().objectForKey("active_group") as PFObject
        
        var memberRelation = activeGroup.relationForKey("members")
        memberRelation.addObject(requester)
        activeGroup.save()
        
        request.delete()
        
        self.loadData()
        
    }
    
    @IBAction func denyClicked(sender :AnyObject!) {
        var request = self.requests[sender.tag]
        request.delete()
        
        self.loadData()
    }
    
    
}