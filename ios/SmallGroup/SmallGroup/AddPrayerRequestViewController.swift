//
//  AddPrayerRequestViewController.swift
//  SmallGroup
//
//  Created by Austin Aldrich on 11/5/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

import Foundation

class AddPrayerRequestViewController: XLFormViewController {
    
    var data :PFObject?
    var editMode = false
    var isPrayerRequestOwner = true
    
    var descriptionRow = XLFormRowDescriptor(tag: "description_row", rowType: XLFormRowDescriptorTypeTextView, title: "Description")
    var peopleSection = XLFormSectionDescriptor()
    var descriptionSection = XLFormSectionDescriptor()
    
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        doInit()
    }
    
    
    override init!(form: XLFormDescriptor!, style: UITableViewStyle) {
        super.init(form: form, style: style)
        doInit()
    }
    
    func doInit() {
        
        var tempForm = XLFormDescriptor()
        
        peopleSection.title = "Who are you praying for?"
        peopleSection.multiValuedTag = "textFieldRow"
        
        descriptionRow.disabled = !isPrayerRequestOwner
        descriptionSection.addFormRow(descriptionRow)
        
        tempForm.addFormSection(peopleSection)
        tempForm.addFormSection(descriptionSection)
        
        self.form = tempForm
    }
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        if(!self.editMode) {
            self.isPrayerRequestOwner = true
            self.data = PFObject(className: "PrayerRequest")
        }
        else {
           setupEditMode()
        }
        
        if(self.isPrayerRequestOwner) {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: UIBarButtonItemStyle.Done, target: self, action: "save")
        }
        
        if(isPrayerRequestOwner) {
            self.peopleSection.isMultivaluedSection = true
            addPersonRow("")
        }
        
        
    }
    
    func setupEditMode() {
        var prayerRequestOwner = self.data!.objectForKey("user") as PFUser
        var currentUser = PFUser.currentUser()
        self.isPrayerRequestOwner = prayerRequestOwner.objectId == currentUser.objectId
        
        self.descriptionRow.value = self.data?["description"] as String
        
        var prayerRecipients = self.data?.objectForKey("prayer_recipients") as? [String]
        
        if let recipients = prayerRecipients {
            for r in recipients {
                var recipientName = r as String
                    addPersonRow(recipientName)
            }
            
        }

    }
    
    func addPersonRow(recipientName :String) {
        var row = XLFormRowDescriptor(tag: "row", rowType: XLFormRowDescriptorTypeText)
        row.cellConfig["textField.placeholder"] = "Add a new person"
        row.value = recipientName
        row.disabled = !self.isPrayerRequestOwner
        peopleSection.addFormRow(row)

    }
    
    
    func setData(data :PFObject, editMode :Bool) {
       self.data = data
       self.editMode = editMode
    }
    
    @IBAction func save() {
        var group = PFUser.currentUser().objectForKey("active_group") as PFObject
        
        self.data?["description"] = self.descriptionRow.value
        self.data?.setObject(PFUser.currentUser(), forKey: "user")
        self.data?.setObject(group, forKey: "group")
        
        var prayerRecipients = [String]()
        
        var tags = [String]()
        
        for tmpPersonRow in self.peopleSection.formRows {
            var personRow = tmpPersonRow as XLFormRowDescriptor
            
            if(personRow.value == nil) { continue }
            
            var personText = personRow.value as String
            
            if(!personText.isEmpty) {
                prayerRecipients.append(personText)
            }
        }
        
        self.data?.setObject(prayerRecipients, forKey: "prayer_recipients")
        
        self.data?.save()
        
        self.navigationController?.popViewControllerAnimated(true)
    }
}
