//
//  NameGenerator.swift
//  SmallGroup
//
//  Created by Austin Aldrich on 1/4/15.
//  Copyright (c) 2015 Missions in Motion. All rights reserved.
//

import Foundation

extension PFUser {

    func getFullName() -> String {
        var firstLastName = getFirstLastName()
        return "\(firstLastName.firstName) \(firstLastName.lastName)"
    }
    
    func getFirstLastName() -> (firstName :String, lastName :String) {
        var firstName = self["first_name"] as? String ?? ""
        var lastName = self["first_name"] as? String ?? ""
        return (firstName, lastName)
    }
    
    
    func getInitials() -> String {
        
        var firstLastName = getFirstLastName()
        
        var firstInitial = firstLastName.firstName[0]
        var lastInitial = firstLastName.lastName[0]
    
        return "\(firstInitial)\(lastInitial)"
        
    }
    
    func getActiveGroup() -> PFObject {
        var group = self.objectForKey("active_group") as PFObject
        group.fetchIfNeeded()
        return group
    }
    
    func getGroups() -> [PFObject] {
       var groupsQuery = PFQuery(className: "Group")
       groupsQuery.whereKey("members", equalTo: self)
       return groupsQuery.findObjects() as [PFObject]
    }
    
    
}