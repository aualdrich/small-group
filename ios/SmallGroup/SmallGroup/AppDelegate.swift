//
//  AppDelegate.swift
//  Small Group
//
//  Created by Austin Aldrich on 9/27/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    let PUSH_TYPE_KEY = "push_type"


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        ChatMessage.registerSubclass()
        
        var parseApplicationId = NSBundle.mainBundle().objectForInfoDictionaryKey("parse_application_id") as String
        var parseClientKey = NSBundle.mainBundle().objectForInfoDictionaryKey("parse_client_key") as String
        
        Parse.setApplicationId(parseApplicationId, clientKey: parseClientKey)
        
        PFAnalytics.trackAppOpenedWithLaunchOptions(launchOptions);
        
        var notificationTypes = UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound
        
        var localNotificationSettings = UIUserNotificationSettings(forTypes: notificationTypes, categories: nil)
        
        application.registerUserNotificationSettings(localNotificationSettings)
        application.registerForRemoteNotificationTypes(UIRemoteNotificationType.Alert | UIRemoteNotificationType.Badge | UIRemoteNotificationType.Sound)
        
        var startStoryboardID = PFUser.currentUser() != nil ? "Authenticated" : "NotAuthenticated"
        
        var storyboard = self.window!.rootViewController?.storyboard!
        
        self.window?.rootViewController = storyboard?.instantiateViewControllerWithIdentifier(startStoryboardID)! as? UIViewController
        
        return true
    }
    

    
    func applicationDidBecomeActive(application: UIApplication) {
        println("resuming")
        if(PFUser.currentUser() != nil) {
            self.syncGroupsForUser()
        }
    }
    
    
    func syncGroupsForUser() {
        
        var currentUser = PFUser.currentUser()
        var username = currentUser.username
        var activeGroup = currentUser.objectForKey("active_group") as PFObject?
        
        var inviteQuery = PFQuery(className: "GroupInvite")
        inviteQuery.whereKey("email", equalTo: username)
        

        var results = inviteQuery.findObjects() as [PFObject]
        
        for invite in results {
            
            var group = invite.objectForKey("group") as PFObject
            var memberRelation = group.relationForKey("members")
            memberRelation.addObject(PFUser.currentUser())
            group.save()
            
            if(activeGroup == nil) {
                currentUser.setObject(group, forKey: "active_group")
                currentUser.save()
            }
            
            var inviteObj = invite as PFObject
            inviteObj.deleteInBackground()
        }
        
        self.syncPushChannelsForUser()
        
    }
    

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        
        var installation = PFInstallation.currentInstallation()
        installation.setDeviceTokenFromData(deviceToken)
        installation.save()
        
        syncPushChannelsForUser()
    }
    
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        
        if userInfo[PUSH_TYPE_KEY] != nil {
            
            var pushType = userInfo[PUSH_TYPE_KEY] as String
            
            if pushType == PushTypes.GroupChat.rawValue {
                handleChatNotification(application)
            }
                
            
        }
    }
    
    
    func handleChatNotification(application :UIApplication) {
        var vc = self.window?.rootViewController
        
        if vc!.isKindOfClass(ECSlidingViewController) {
            
            var sliding = vc! as ECSlidingViewController
            
            //notification tapped from background - take the user to the chat screen
            if(application.applicationState == UIApplicationState.Background || application.applicationState == UIApplicationState.Inactive ) {
               self.chatNotificationTappedFromBackground(sliding)
            }
                
            else {
                self.chatNotificationTappedFromForeground(sliding)
            }
            
        }
    }
    
    func chatNotificationTappedFromBackground(sliding :ECSlidingViewController) {
        var chatViewController = self.window?.rootViewController?.storyboard?.instantiateViewControllerWithIdentifier("Chat") as UINavigationController
        
        sliding.topViewController = chatViewController
    }
    
    func chatNotificationTappedFromForeground(sliding :ECSlidingViewController) {
        if sliding.topViewController.isKindOfClass(UINavigationController) {
            
            var navController = sliding.topViewController as UINavigationController
            
            
            if navController.topViewController.isKindOfClass(ChatViewController) {
                
                var chatController = navController.topViewController as ChatViewController
                
                chatController.getLatestMessages()
                
                
            }
            
            
        }
    }
    
    func syncPushChannelsForUser() {
        
        var channels = [String]()
        
        var user = PFUser.currentUser()
        
        if(user != nil) {
            var groups = user.getGroups()
            
            for group in groups {
                
                var groupChannelName = PushChannelFormatters.getGroupChannelName(group.objectId)
                
                channels.append(groupChannelName)
            }
        }
        
        var installation = PFInstallation.currentInstallation()
        installation.channels = channels
        installation.save()
    }
    
    
    func userLoggedIn() {
        var storyboard = self.window!.rootViewController?.storyboard!
        var home = storyboard?.instantiateViewControllerWithIdentifier("Authenticated")! as? ECSlidingViewController
        home?.topViewController = storyboard?.instantiateViewControllerWithIdentifier("Me") as UIViewController
        self.window?.rootViewController = home
        
        self.syncGroupsForUser()
        

    }


}

