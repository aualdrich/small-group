//
//  FindGroupResultsViewController.swift
//  SmallGroup
//
//  Created by Austin Aldrich on 12/24/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

import Foundation

class FindGroupResultsViewController : UITableViewController {
    
    var searchResults = [PFObject]()
    var selectedGroup :PFObject?
    
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResults.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCellWithIdentifier("cell") as? UITableViewCell
        
        if(cell == nil) {
            cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "cell")
        }
        
        var item = self.searchResults[indexPath.row]
        
        cell?.textLabel?.text = item["name"] as? String
        cell?.detailTextLabel?.text = item["org_name"] as? String
        
        return cell!
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        var item = self.searchResults[indexPath.row]
        self.selectedGroup = item
        
        var groupResult = self.storyboard?.instantiateViewControllerWithIdentifier("GroupResult") as GroupResultViewController
        
        groupResult.group = item
        
        self.presentingViewController?.navigationController?.pushViewController(groupResult, animated: true)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "GroupResultSegue") {
            var controller = segue.destinationViewController as GroupResultViewController
            controller.group = self.selectedGroup!
        }
    }
    
    
    
}
