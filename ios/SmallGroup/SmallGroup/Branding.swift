//
//  Branding.swift
//  SmallGroup
//
//  Created by Austin Aldrich on 10/2/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

import Foundation

class Branding {
    
    class func getBlueColor() -> UIColor {
        return UIColor(red: 0.34, green: 0.78, blue: 0.82, alpha: 1.0)
    }
    
    class func getDarkestGreyColor() -> UIColor {
       return UIColor(red: 0.47, green: 0.48, blue: 0.49, alpha: 1.0)
    }
    
    class func getDarkGreyColor() -> UIColor {
       return UIColor(red: 0.60, green: 0.62, blue: 0.64, alpha: 1.0)
    }
    
    class func getLightGreyColor() -> UIColor {
       return UIColor(red: 0.89, green: 0.90, blue: 0.91, alpha: 1.0)
    }
    
    class func getRedColor() -> UIColor {
       return UIColor(red: 0.80, green: 0.42, blue: 0.10, alpha: 1.0)
    }
    
    class func getDarkGreenColor() -> UIColor {
       return UIColor(red: 0.16, green: 0.65, blue: 0.08, alpha: 1.0)
    }
    
    
    class func getLightGreenColor() -> UIColor {
       return UIColor(red: 0.46, green: 0.84, blue: 0.10, alpha: 1.0)
    }
    
    class func getSystemBlueColor() -> UIColor {
        return UIColor(red: 0, green: 0.478431, blue: 1.0, alpha: 1.0)
    }
    
}



