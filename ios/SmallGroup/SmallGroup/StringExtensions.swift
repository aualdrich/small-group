//
//  StringExtensions.swift
//  SmallGroup
//
//  Created by Austin Aldrich on 1/6/15.
//  Copyright (c) 2015 Missions in Motion. All rights reserved.
//

import Foundation

extension String {
    subscript (i: Int) -> String {
        return String(Array(self)[i])
    }
}