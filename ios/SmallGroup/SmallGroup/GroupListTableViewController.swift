//
//  GroupListTableViewController.swift
//  SmallGroup
//
//  Created by Austin Aldrich on 10/4/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

import UIKit

class GroupListTableViewController: PFQueryTableViewController, UIActionSheetDelegate {
    
    var currentGroup :PFObject?
    var editMode = false
    
    let FIND_GROUP_BUTTON_INDEX = 1
    let ADD_GROUP_BUTTON_INDEX  = 2
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    
    override func viewWillAppear(animated: Bool) {
        
        self.loadObjects()
        
    }

    override func queryForTable() -> PFQuery! {
        
        var query = PFQuery(className: "Group")
        query.whereKey("members", equalTo: PFUser.currentUser())
        
        return query
        
    }
    
    override func tableView(tableView: UITableView!, cellForRowAtIndexPath indexPath: NSIndexPath!, object: PFObject!) -> PFTableViewCell! {
        
        var cell = tableView.dequeueReusableCellWithIdentifier("Cell") as? PFTableViewCell
        
        if(cell == nil) {
            cell = PFTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "Cell")
        }
            
        cell?.textLabel?.text = object["name"] as? String
        
        var activeGroup = PFUser.currentUser().objectForKey("active_group") as? PFObject
        
        
        if(activeGroup?.objectId == object.objectId) {
            cell?.accessoryType = UITableViewCellAccessoryType.Checkmark
        }
        
        else {
             cell?.accessoryType = UITableViewCellAccessoryType.None
        }
        
        return cell
        
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if(editingStyle == UITableViewCellEditingStyle.Delete) {
         
        }
        
    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    
    override func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [AnyObject]? {
        var editAction = UITableViewRowAction(style: UITableViewRowActionStyle.Normal, title: "Edit") { (action: UITableViewRowAction!, indexPath: NSIndexPath!) -> Void in
            
            self.currentGroup = self.objects[indexPath.row] as? PFObject
            self.editMode = true
            
            self.performSegueWithIdentifier("AddGroupSegue", sender: self)
            
           
        }
        
        var deleteAction = UITableViewRowAction(style: UITableViewRowActionStyle.Default, title: "Delete", handler: { (action :UITableViewRowAction!, indexPath: NSIndexPath!) -> Void in
                var itemToDelete = self.objects[indexPath.row] as PFObject
                itemToDelete.delete()
                self.loadObjects()
            })
        
        deleteAction.backgroundColor = UIColor.redColor()
        
//        var items = [deleteAction, editAction]
        var items = [editAction]
        
        return items
        
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var group = self.objects[indexPath.row] as PFObject
        PFUser.currentUser().setObject(group, forKey:"active_group")
        PFUser.currentUser().save()
        
        self.tableView.reloadData()
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if(segue.identifier == "AddGroupSegue") {
            var dest = segue.destinationViewController as AddGroupViewController
            
            if(self.editMode) {
                dest.setData(true, group: self.currentGroup!)
            }
            
            self.editMode = false //reset edit mode to off until next time user swips and taps "edit"
        }
    }
    
    @IBAction func addGroupClicked() {
        
        var sheet = UIActionSheet(title: "Add or Find a Group", delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Find a group", "Add a new group")
        
        sheet.showFromBarButtonItem(self.navigationItem.rightBarButtonItem, animated: true)
        
    }
    
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int) {
        
        switch(buttonIndex) {
            case FIND_GROUP_BUTTON_INDEX:
                self.performSegueWithIdentifier("FindGroupSegue", sender: self)
            case ADD_GROUP_BUTTON_INDEX:
                self.performSegueWithIdentifier("AddGroupSegue", sender: self)
            default:
                break;
        }
        
        
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView!, canEditRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView!, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath!) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView!, moveRowAtIndexPath fromIndexPath: NSIndexPath!, toIndexPath: NSIndexPath!) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView!, canMoveRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */
    
}
