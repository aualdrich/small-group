//
//  GroupDetailsViewController.swift
//  SmallGroup
//
//  Created by Austin Aldrich on 11/23/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

import Foundation

class GroupDetailsViewController : UIViewController {
    
    var group :PFObject?
    @IBOutlet var navItem :UINavigationItem?
    @IBOutlet var groupNameLabel :UILabel?
    @IBOutlet var groupSubtitleLabel :UILabel?
    @IBOutlet var groupPurposeTextView :UITextView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.group = PFUser.currentUser().objectForKey("active_group") as? PFObject
        
        if(self.group == nil) {
            var groupSelector = self.storyboard?.instantiateViewControllerWithIdentifier("GroupListNavigationController") as UIViewController
            self.slidingViewController().topViewController = groupSelector
        }
        
        else {
            self.group?.fetchIfNeeded()
        
            navItem?.title = self.group!["name"] as? String
            
            self.groupNameLabel?.text = group!["name"] as? String
            
            var location = group!["location"] as? String
            var organization = group!["org_name"] as? String
            
            if(location == nil) { location = "?" }
            if(organization == nil) { organization = "" }
            
            var subTitle = "\(organization!) - \(location!)"
            
            self.groupSubtitleLabel?.text = subTitle
            
            self.groupPurposeTextView?.text = group!["purpose"] as? String

        }
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if(segue.identifier == "EditGroupSegue") {
            var destController = segue.destinationViewController as AddGroupViewController
            destController.setData(true, group: group!)
        }
        
        
    }
    
}