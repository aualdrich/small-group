//
//  MemberInviteViewController.swift
//  SmallGroup
//
//  Created by Austin Aldrich on 10/11/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

import UIKit
import MessageUI




class MemberInviteViewController: ButtonPeoplePicker, ButtonPeoplePickerDelegate, MemberInviteSelectionMethodDelegate, MFMailComposeViewControllerDelegate {
    
    var currentRecord :APContact?
    var invites = [PFObject]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self;

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func buttonPeoplePickerDidCancel(buttonPeoplePicker: ButtonPeoplePicker!) {
        self.dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    
    func buttonPeoplePickerDidFinish(buttonPeoplePicker: ButtonPeoplePicker!, withRecords records: NSOrderedSet!) {
        sendEmails(self.invites)
    }
    
    func sendEmails(emailInvites :[PFObject]) {
       
        var messageController = MFMailComposeViewController()
        messageController.mailComposeDelegate = self
        messageController.setSubject("You've been invited to join a small group!")
        
        
        var toRecipients = emailInvites.map { (var invite) -> String in
            return invite["email"] as String
        }
        
        var count = toRecipients.count
        var count2 = emailInvites.count
        
        messageController.setToRecipients(toRecipients)
        
        var invitedByFirstName = PFUser.currentUser()["first_name"] as String?
        if(invitedByFirstName == nil) {
            invitedByFirstName = ""
        }
        
        var invitedByLastName = PFUser.currentUser()["last_name"] as String?
        if(invitedByLastName == nil) {
            invitedByLastName = ""
        }
        
        var currentGroup = PFUser.currentUser().objectForKey("active_group") as PFObject
        var currentGroupName = currentGroup["name"] as String
        
        var messageBody = "\(invitedByFirstName!) \(invitedByLastName!) has invited you " +
            "to the \(currentGroupName) small group. Sign up or login to the SmallGroup app with this email address to join the group."
        messageController.setMessageBody(messageBody, isHTML: false)
        self.presentViewController(messageController, animated: true, completion: nil)
        
    }
    
    func mailComposeController(controller: MFMailComposeViewController!, didFinishWithResult result: MFMailComposeResult, error: NSError!) {
       
        self.dismissViewControllerAnimated(true, completion: nil)
        self.dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    func buttonPeoplePickerDidSelectPerson(buttonPeoplePicker: ButtonPeoplePicker!, _ record: APContact!) {
        self.currentRecord = record
        self.performSegueWithIdentifier("InviteSelection", sender: self)
        
    }
        
        
    
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "InviteSelection") {
            var nav = segue.destinationViewController as UINavigationController
            var invite = nav.topViewController as MemberInviteSelectionMethodViewController
            invite.delegate = self
            invite.setData(self.currentRecord!)
        }
    }
    
    func inviteMethodSelected(email: String, contact :APContact) {
        
       var invite = PFObject(className: "GroupInvite")
        
        var currentGroup = PFUser.currentUser().objectForKey("active_group") as PFObject
        
        invite["group"] = currentGroup
        invite["email"] = email
        invite.setObject(PFUser.currentUser(), forKey: "invited_by")
        invite["first_name"] = contact.firstName
        invite["last_name"] = contact.lastName == nil ? "" : contact.lastName
        invite["record_id"] = contact.recordID
        
        invite.saveInBackgroundWithBlock { (success :Bool, error :NSError!) -> Void in
            self.invites.append(invite)
        }
        
        
        
    }
    
    func inviteMethodCanceled() {
        
    }
    
    func buttonPeoplePickerDidRemovePerson(buttonPeoplepicker: ButtonPeoplePicker!, _ record: APContact!) {
       
        var indexToDelete = -1
        
        for (index, invite) in enumerate(self.invites) {
            var recordID = invite["record_id"] as NSNumber
            
            if(record.recordID.isEqualToNumber(recordID)) {
               indexToDelete = index
               break
            }
            
        }
        
        if(indexToDelete > -1) {
            self.invites.removeAtIndex(indexToDelete)
        }
        
        
    }
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
