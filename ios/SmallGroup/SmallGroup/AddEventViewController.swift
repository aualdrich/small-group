//
//  AddEventViewController.swift
//  SmallGroup
//
//  Created by Austin Aldrich on 12/2/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

import Foundation


class AddEventViewController : XLFormViewController  {
    
    var nameRow = XLFormRowDescriptor(tag: "name", rowType: XLFormRowDescriptorTypeText, title: "Name")
    
    var descriptionRow = XLFormRowDescriptor(tag: "description", rowType: XLFormRowDescriptorTypeTextView, title: "Description")
    
    var locationRow = XLFormRowDescriptor(tag: "location", rowType: XLFormRowDescriptorTypeText, title: "Location")
    
    var startDateRow = XLFormRowDescriptor(tag: "start_date", rowType: XLFormRowDescriptorTypeDateInline, title: "Start Date")
    
    var startTimeRow = XLFormRowDescriptor(tag: "start_time", rowType: XLFormRowDescriptorTypeTimeInline, title: "Start Time")
    
    var endDateRow = XLFormRowDescriptor(tag: "end_date", rowType: XLFormRowDescriptorTypeDateInline, title: "End Date")
    
    var endTimeRow = XLFormRowDescriptor(tag: "end_time", rowType: XLFormRowDescriptorTypeTimeInline, title: "End Time")
    
    var dateFormatter = NSDateFormatter()
    var timeFormatter = NSDateFormatter()
    var startTimePicker = UIDatePicker()
    
    var event = PFObject(className: "Event")
    var editMode = false
    
    
    required init(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        
        self.dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
        self.dateFormatter.timeStyle = NSDateFormatterStyle.NoStyle
        
        self.timeFormatter.dateStyle = NSDateFormatterStyle.NoStyle
        self.timeFormatter.timeStyle = NSDateFormatterStyle.ShortStyle
        
        self.buildForm()
        
    }
    
    func buildForm() {
        var form = XLFormDescriptor()
        var section = XLFormSectionDescriptor()
        
        section.addFormRow(nameRow)
        section.addFormRow(descriptionRow)
        section.addFormRow(locationRow)
        section.addFormRow(startDateRow)
        section.addFormRow(startTimeRow)
        section.addFormRow(endDateRow)
        section.addFormRow(endDateRow)
        section.addFormRow(endTimeRow)
        
        form.addFormSection(section)
        
        self.form = form
        
        self.nameRow.value = ""
        self.descriptionRow.value = ""
        self.locationRow.value = ""
        self.startDateRow.value = NSDate()
        self.startTimeRow.value = NSDate()
        self.endDateRow.value = NSDate()
        self.endTimeRow.value = NSDate()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if(self.editMode) {
            self.intializeNewFormMode()
        }
        
        else {
            self.navigationController?.toolbarHidden = true
        }
        
    }
    
    func intializeNewFormMode() {
        self.navigationItem.title = "Edit Event"
        
        self.nameRow.value = self.event["name"] as? String
        self.descriptionRow.value = self.event["description"] as? String
        self.locationRow.value = self.event["location"] as? String
        
        var startDate = self.event["start_date"] as? NSDate
        var endDate = self.event["end_date"] as? NSDate
        
        if(startDate != nil) {
            self.startDateRow.value = startDate!
            self.startTimeRow.value = startDate
        }
        
        if(endDate != nil) {
            self.endDateRow.value = endDate!
            self.endTimeRow.value = endDate!
        }
        
        self.navigationController?.toolbarHidden = false
        var addToCalendarToolbarButton = UIBarButtonItem(title: "Add to Calendar", style: UIBarButtonItemStyle.Plain, target: self, action: "addToCalendar")
        
        var flexibleSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: "")
        
        var deleteEventButton = UIBarButtonItem(image: UIImage(named: "Delete"), style: UIBarButtonItemStyle.Plain, target: self, action: "deleteEvent")
        
        
        self.toolbarItems = [addToCalendarToolbarButton, flexibleSpace, deleteEventButton]
        
        
        
    }
    
    @IBAction func addToCalendar() {
        
        var store = EKEventStore()
        
        store.requestAccessToEntityType(EKEntityTypeEvent, completion: { (granted :Bool, error :NSError!) -> Void in
           
            if(!granted) {
                return
            }
            
            var calendarEvent = EKEvent(eventStore: store)
            calendarEvent.title = self.event["name"] as String
            calendarEvent.startDate = self.event["start_date"] as NSDate
            calendarEvent.endDate = self.event["end_date"] as NSDate
            
            calendarEvent.calendar = store.defaultCalendarForNewEvents
            
            var error :NSError?
            
            store.saveEvent(calendarEvent, span: EKSpanThisEvent, commit: true, error: &error)
            
            var alertView = UIAlertView(title: "", message: "Added to calendar!", delegate: self, cancelButtonTitle: "Ok")
            
            alertView.show()
            
            //self.view.makeToast("Added to calendar!", duration: 2.0, position: "center")
            
        
       })
        
        
        
        
    }
    
    @IBAction func deleteEvent() {
        
        self.event.delete()
        
        self.navigationController?.popViewControllerAnimated(true)
        
        
    }
    
    @IBAction func save() {
        
        var validForm = self.doValidations()
        
        if(!validForm) { return }
        
        event.setObject(PFUser.currentUser(), forKey: "created_by")
        
        var group = PFUser.currentUser().objectForKey("active_group") as PFObject
        
        event.setObject(group, forKey: "group")
        event["name"] = self.nameRow.value
        event["description"] = self.descriptionRow.value
        event["location"] = self.locationRow.value
        
        var startDate = self.startDateRow.value as NSDate
        var startTime = self.startTimeRow.value as NSDate
        var startDateAndTime = getCombinedDates(startDate, time: startTime)
        event["start_date"] = startDateAndTime
        
        var endDate = self.endDateRow.value as NSDate
        var endTime = self.endTimeRow.value as NSDate
        var endDateAndTime = getCombinedDates(endDate, time: endTime)
        event["end_date"] = endDateAndTime
        
        event.save()
        
        self.navigationController?.popViewControllerAnimated(true)
        
        
    }
    
    func getCombinedDates(date :NSDate, time :NSDate) -> NSDate {
        

        var calendar = NSCalendar.currentCalendar()

        var dateComponents = calendar.components(NSCalendarUnit.DayCalendarUnit|NSCalendarUnit.MonthCalendarUnit|NSCalendarUnit.YearCalendarUnit, fromDate: date)
        
        
        var timeComponents = calendar.components(NSCalendarUnit.HourCalendarUnit|NSCalendarUnit.MinuteCalendarUnit, fromDate: time)
        
        
        var newComponents = NSDateComponents()
        newComponents.timeZone = NSTimeZone.systemTimeZone()
        newComponents.day = dateComponents.day
        newComponents.month = dateComponents.month
        newComponents.year = dateComponents.year
        newComponents.hour = timeComponents.hour
        newComponents.minute = timeComponents.minute
        
        var combinedDate = calendar.dateFromComponents(newComponents)
        
        return combinedDate!
    }
    
}
