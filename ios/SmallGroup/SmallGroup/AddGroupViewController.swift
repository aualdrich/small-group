//
//  AddGroupViewController.swift
//  SmallGroup
//
//  Created by Austin Aldrich on 10/5/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

import UIKit

class AddGroupViewController: XLFormViewController {
    
    var editMode = false
    var group :PFObject?
    
    var nameRow = XLFormRowDescriptor(tag: "name", rowType: XLFormRowDescriptorTypeText, title: "Group Name")
    
    var organizationRow = XLFormRowDescriptor(tag: "organization", rowType: XLFormRowDescriptorTypeText, title: "Organization")
    
    var purposeRow = XLFormRowDescriptor(tag: "purpose", rowType: XLFormRowDescriptorTypeTextView, title: "Purpose")
    
    var locationRow = XLFormRowDescriptor(tag: "location", rowType: XLFormRowDescriptorTypeText, title: "Location")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var form = XLFormDescriptor()
        var section = XLFormSectionDescriptor()
        
        nameRow.required = true
        organizationRow.required = true
        purposeRow.required = true
        locationRow.required = true
        
        section.addFormRow(nameRow)
        section.addFormRow(organizationRow)
        section.addFormRow(purposeRow)
        section.addFormRow(locationRow)
        
        form.addFormSection(section)
        
        self.form = form
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setData(editMode :Bool, group :PFObject) {
        
        self.editMode = editMode;
        self.group = group;
        
        self.nameRow.value = group["name"] as String
        
        self.organizationRow.value = group["org_name"] as? String
        self.locationRow.value = group["location"] as? String
        self.purposeRow.value = group["purpose"] as? String
        
        self.navigationItem.title = "Edit Group"
        
    }
    
    @IBAction func save() {
        
        var validForm = self.doValidations()
        
        if(!validForm) { return }
        
        
        var formData = ["name": self.nameRow.value, "org_name": self.organizationRow.value, "location": self.locationRow.value, "purpose": self.purposeRow.value]
        
        if(!self.editMode) {
            self.group = PFObject(className: "Group", dictionary: formData)
            var membersRelation = self.group!.relationForKey("members")
            membersRelation.addObject(PFUser.currentUser())
            self.group!.save()
        }
        
        else {
            self.group!["name"] = self.nameRow.value
            self.group!["org_name"] = self.organizationRow.value
            self.group!["location"] = self.locationRow.value
            self.group!["purpose"] = self.purposeRow.value
            
            self.group?.save()
        }
        
        self.navigationController!.popViewControllerAnimated(true)
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
