//
//  XLFormExtensions.swift
//  SmallGroup
//
//  Created by Austin Aldrich on 1/1/15.
//  Copyright (c) 2015 Missions in Motion. All rights reserved.
//

import Foundation

extension XLFormViewController {
    
    func doValidations() -> Bool {
        var validationErrors = self.formValidationErrors()
        
        var isValid = validationErrors.count == 0
        
        if !isValid {
            for tmpError in validationErrors {
                var error = tmpError as NSError
                self.showFormValidationError(error)
            }
            
            return false
        }
        
        return true
    }
    
    
    
    
    
    
}