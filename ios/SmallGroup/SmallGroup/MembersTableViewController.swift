//
//  MembersTableViewController.swift
//  Small Group
//
//  Created by Austin Aldrich on 9/27/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

import UIKit

class MembersTableViewController: PFQueryTableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        
    }
    
    override func queryForTable() -> PFQuery! {
        var activeGroup = PFUser.currentUser().objectForKey("active_group") as PFObject
        activeGroup.fetchIfNeeded()
        
        var members = activeGroup.relationForKey("members")
        var query = members.query()
        query.orderByAscending("first_name")
        return query
    }
    
    override func tableView(tableView: UITableView!, cellForRowAtIndexPath indexPath: NSIndexPath!, object: PFObject!) -> PFTableViewCell! {
        
        var cell = tableView.dequeueReusableCellWithIdentifier("Cell") as PFTableViewCell!
        
        if(cell == nil) {
            cell = PFTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "Cell")
        }
        
        var firstName = object["first_name"] as String?
        var lastName = object["last_name"] as String?
        
        if(firstName == nil) {
            firstName = ""
        }
        
        if(lastName == nil) {
            lastName = ""
        }
        
        
        if(firstName == "" && lastName == "") {
           cell.textLabel?.text = object.objectForKey("username") as? String
        }
        
        else {
            cell.textLabel?.text = "\(firstName!) \(lastName!)"
        }
        
        return cell
    }
    
}
