//
//  LoginViewController.swift
//  SmallGroup
//
//  Created by Austin Aldrich on 10/2/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

import UIKit

class LoginViewController: PFLogInViewController, PFLogInViewControllerDelegate, PFSignUpViewControllerDelegate {
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        self.emailAsUsername = true
        self.signUpController.emailAsUsername = true
        
        self.delegate = self;
        self.signUpController.delegate = self
        
        
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        var loginLogoView = UIImageView(image: UIImage(named: "small_group_logo.png"))
        loginLogoView.contentMode = UIViewContentMode.ScaleAspectFit
        self.logInView.logo = loginLogoView
        
        self.logInView.backgroundColor = UIColor.whiteColor()
        
        self.logInView.usernameField.backgroundColor = Branding.getLightGreyColor()
        self.logInView.usernameField.textColor = Branding.getDarkGreyColor()
        
        self.logInView.passwordField.backgroundColor = Branding.getLightGreyColor()
        self.logInView.passwordField.textColor = Branding.getDarkGreyColor()
        
        self.logInView.logInButton.backgroundColor = Branding.getDarkestGreyColor()
        self.logInView.logInButton.setBackgroundImage(nil, forState: UIControlState.Normal)
        
        self.logInView.signUpButton.backgroundColor = Branding.getDarkGreenColor()
        self.logInView.signUpButton.setBackgroundImage(nil, forState: UIControlState.Normal)
        
        
        var signUpLogoView = UIImageView(image: UIImage(named: "small_group_logo.png"))
        signUpLogoView.contentMode = UIViewContentMode.ScaleAspectFit
        self.signUpController.signUpView.logo = signUpLogoView
        self.signUpController.signUpView.backgroundColor = UIColor.whiteColor()
        //self.signUpController.signUpView.logo = logoView
        self.signUpController.signUpView.usernameField.backgroundColor = Branding.getLightGreyColor()
        self.signUpController.signUpView.usernameField.textColor = Branding.getDarkGreyColor()
        self.signUpController.signUpView.usernameField.borderStyle = UITextBorderStyle.None
        self.signUpController.signUpView.passwordField.backgroundColor = Branding.getLightGreyColor()
        self.signUpController.signUpView.passwordField.textColor = Branding.getDarkGreyColor()
        self.signUpController.signUpView.signUpButton.backgroundColor = Branding.getDarkGreenColor()
        self.signUpController.signUpView.signUpButton.setBackgroundImage(nil, forState: UIControlState.Normal)
        
    }
    
    
    func logInViewController(logInController: PFLogInViewController!, didLogInUser user: PFUser!) {
        var delegate = UIApplication.sharedApplication().delegate as AppDelegate
        delegate.userLoggedIn()
        
    }
    
    func signUpViewController(signUpController: PFSignUpViewController!, didSignUpUser user: PFUser!) {
        
        user.email = user.username
        user.save()
        
        var delegate = UIApplication.sharedApplication().delegate as AppDelegate
        delegate.userLoggedIn()
    }
    
    
    
    
    
}
