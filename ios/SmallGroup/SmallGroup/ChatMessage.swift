//
//  ChatMessage.swift
//  SmallGroup
//
//  Created by Austin Aldrich on 1/4/15.
//  Copyright (c) 2015 Missions in Motion. All rights reserved.
//

import Foundation

class ChatMessage : PFObject, JSQMessageData, PFSubclassing {
    
    
    class func parseClassName() -> String! {
        return "ChatMessage"
        
    }
    
    
    func senderId() -> String! {
        var user = self.objectForKey("sender") as PFUser
        return user.objectId
    }
    
    func senderDisplayName() -> String! {
        var user = self.objectForKey("sender") as PFUser
        return user.getFullName()
    }
    
    func date() -> NSDate! {
        return self.createdAt
    }
    
    func media() -> JSQMessageMediaData! {
        return ChatMediaData()
    }
    
    func isMediaMessage() -> Bool {
//        return self["is_media_message"] as Bool
        return false
    }
    
    func hash() -> UInt {
        
        var mediaHash = self.media().hash
        var textHash = self.text().hash
        var senderIDHash = self.senderId().hash
        var dateHash = self.date().hash
        
        var isMediaMessage = self.isMediaMessage()
        
        var contentHash = isMediaMessage ? mediaHash : textHash
        var x = senderIDHash ^ dateHash ^ contentHash
        if x < 0 { return UInt(x * -1) }
    
        return UInt(x)
    }
    
    
    func text() -> String! {
        return self["text"] as String
    }
    
    
    
    
    
    
}