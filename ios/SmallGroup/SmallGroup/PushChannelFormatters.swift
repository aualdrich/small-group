//
//  PushChannelFormatters.swift
//  SmallGroup
//
//  Created by Austin Aldrich on 1/11/15.
//  Copyright (c) 2015 Missions in Motion. All rights reserved.
//

import Foundation

class PushChannelFormatters {
    
    class func getGroupChannelName(groupID :String) -> String {
        return "group_\(groupID)"
    }
}