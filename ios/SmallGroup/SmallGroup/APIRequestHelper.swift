//
//  APIRequestHelper.swift
//  SmallGroup
//
//  Created by Austin Aldrich on 9/30/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

import Foundation

class APIRequestHelper {
    
    class func getBaseURL() -> String {
        var settings = NSBundle.mainBundle().infoDictionary?["APIBaseURL"]! as String
        return settings
    }
    
    class func buildURL(request :String) -> String {
        return getBaseURL() + "/" + request
    }
    
    
}