//
//  FindGroupViewController.swift
//  SmallGroup
//
//  Created by Austin Aldrich on 12/24/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

import Foundation

class FindGroupViewController : UITableViewController, UISearchResultsUpdating, UISearchBarDelegate {
    
    var searchController :UISearchController?
    var groups = [String]()
    var searchResults = [PFObject]()
    @IBOutlet var searchBar :UISearchBar?
    
    
    override func viewDidLoad() {
    
        var resultsNavController = self.storyboard?.instantiateViewControllerWithIdentifier("GroupSearchResultsNavController") as UINavigationController
        
        
        self.searchController = UISearchController(searchResultsController: resultsNavController)
        
        
        self.searchController?.searchResultsUpdater = self
        
        self.searchController?.searchBar.sizeToFit()
        self.tableView.tableHeaderView = self.searchController!.searchBar;
        self.searchController?.searchBar.delegate = self
        
        self.definesPresentationContext = true
        
    }
    
    
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        
        var searchString = self.searchController?.searchBar.text
        
        if (self.searchController!.searchResultsController != nil) {
            
            var navController = self.searchController?.searchResultsController as UINavigationController
            
            var nameQuery = PFQuery(className: "Group")
            nameQuery.whereKey("name", matchesRegex: searchString, modifiers: "i")
            
            var orgQuery = PFQuery(className: "Group")
            orgQuery.whereKey("org_name", matchesRegex: searchString, modifiers: "i")
            
            var searchQuery = PFQuery.orQueryWithSubqueries([nameQuery, orgQuery])
            
            self.searchResults.removeAll(keepCapacity: true)
            
            var searchResults = searchQuery.findObjects() as? [PFObject]
            
            if(searchResults == nil) { return }
            
            for result in searchResults! {
               self.searchResults.append(result)
            }
            
            var findGroupResults = navController.topViewController as FindGroupResultsViewController
            
            findGroupResults.searchResults = self.searchResults
            findGroupResults.tableView.reloadData()
          
        }
        
        
    }

    
    
    
}
