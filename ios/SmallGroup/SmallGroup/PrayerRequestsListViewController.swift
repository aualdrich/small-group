//
//  PrayerRequestsListViewController.swift
//  SmallGroup
//
//  Created by Austin Aldrich on 11/5/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

import Foundation

class PrayerRequestsListViewController :UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var mePrayerViewIndex = 0
    var groupPrayerViewIndex = 1
    
    @IBOutlet var prayerRequestsTableView :UITableView?
    @IBOutlet var prayerViewSegment :UISegmentedControl?
    
    var prayerRequests = [PFObject]()
    
    var currentPrayerRequest :PFObject?
    
    override func viewDidLoad() {
        
    }
    
    override func viewWillAppear(animated: Bool) {
        prayerViewChanged()
        
        super.viewWillAppear(animated)
        
    }
    
    func updateWithMePrayerView() {
        var activeGroup = PFUser.currentUser().objectForKey("active_group") as PFObject
        
        var query = PFQuery(className: "PrayerRequest")
        query.whereKey("user", equalTo: PFUser.currentUser())
        query.whereKey("group", equalTo: activeGroup)
        
        query.findObjectsInBackgroundWithBlock { (results :[AnyObject]!, error :NSError!) -> Void in
           self.prayerRequests = results as [PFObject]
           self.prayerRequestsTableView?.reloadData()
        }
        
    }
    
    func updateWithGroupPrayerView() {
        
        var query = PFQuery(className: "PrayerRequest")
        
        var activeGroup = PFUser.currentUser().objectForKey("active_group") as PFObject
        activeGroup.fetchIfNeeded()
        
        query.whereKey("group", equalTo: activeGroup)
        
        query.findObjectsInBackgroundWithBlock { (results :[AnyObject]!, error :NSError!) -> Void in
            self.prayerRequests = results as [PFObject]
            self.prayerRequestsTableView?.reloadData()
        }
        
    }
    
    @IBAction func prayerViewChanged() {
        
        switch(prayerViewSegment!.selectedSegmentIndex) {
        case mePrayerViewIndex:
            self.updateWithMePrayerView()
        case groupPrayerViewIndex:
            self.updateWithGroupPrayerView()
        default:
            self.updateWithMePrayerView()
        }
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var prayerRequest = self.prayerRequests[indexPath.row]
        
        var cell = tableView.dequeueReusableCellWithIdentifier("cell") as UITableViewCell!
        
        if(cell == nil) {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
        }
        
        cell.textLabel?.text = prayerRequest["description"] as? String
        cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        
        return cell
        
        
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        self.currentPrayerRequest = self.prayerRequests[indexPath.row]
        self.performSegueWithIdentifier("EditPrayerRequestSegue", sender: self)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.prayerRequests.count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
    
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [AnyObject]? {
        
        
        var deleteAction = UITableViewRowAction(style: UITableViewRowActionStyle.Default, title: "Delete", handler: { (action :UITableViewRowAction!, indexPath: NSIndexPath!) -> Void in
            
            var prayerRequest = self.prayerRequests[indexPath.row]
            prayerRequest.deleteInBackground()
            self.prayerRequests.removeAtIndex(indexPath.row)
            tableView.reloadData()
            
        })
        
        deleteAction.backgroundColor = UIColor.redColor()
        
        var items = [deleteAction]
        
        return items
        
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        super.prepareForSegue(segue, sender: sender)
        
        if(segue.identifier == "EditPrayerRequestSegue") {
            var addPrayerRequest = segue.destinationViewController as AddPrayerRequestViewController
            
            addPrayerRequest.setData(self.currentPrayerRequest!, editMode: true)
        }
    }
    
    
    
}