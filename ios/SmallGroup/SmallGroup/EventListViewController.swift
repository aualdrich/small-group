//
//  EventListViewController.swift
//  SmallGroup
//
//  Created by Austin Aldrich on 12/2/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

import Foundation

class EventListViewController : UITableViewController {
    
    var dateFormatter = NSDateFormatter()
    var sectionDateFormatter = NSDateFormatter()
    
    var selectedEvent :PFObject?
    
    var events = Dictionary<String, [PFObject]>()
    
    override func viewWillAppear(animated: Bool) {
       self.navigationController?.toolbarHidden = true
       self.getData()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dateFormatter.dateFormat = "EEE, MMM. d yyyy h:mm a"
        sectionDateFormatter.dateFormat = "MMM. yyyy"
        
    }
    
    func getData() {
        
        self.events.removeAll(keepCapacity: false)
        
        var query = PFQuery(className: "Event")
        
        var group = PFUser.currentUser().objectForKey("active_group") as PFObject
        
        var now = NSDate()
        
        query.whereKey("group", equalTo: group)
        query.whereKey("end_date", greaterThan: now)
        query.orderByAscending("start_date")
        
        
        var tempEvents = query.findObjects() as [PFObject]
        
        for event in tempEvents {
           
            var startDate = event["start_date"] as? NSDate
            
            if(startDate == nil) { continue }
            
            var startDateString = self.sectionDateFormatter.stringFromDate(startDate!)
            
            if self.events.indexForKey(startDateString) == nil {
               self.events.updateValue([event], forKey: startDateString)
            }
            
            else {
                var eventsForKey = self.events[startDateString]
                eventsForKey?.append(event)
                self.events.updateValue(eventsForKey!, forKey: startDateString)
            }
            
        }
        
        self.tableView.reloadData()
        
        var sections = NSIndexSet(indexesInRange: NSMakeRange(0, self.events.keys.array.count))
        
        self.tableView.reloadSections(sections, withRowAnimation: UITableViewRowAnimation.Automatic)
        
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       var events = self.getEventsBySectionIndex(section)
       return events.count
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCellWithIdentifier("cell") as UITableViewCell?
        
        
        if(cell == nil) {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
        }
        
        var event = self.getEventByIndexPath(indexPath)
        
        cell?.textLabel?.text = event["name"] as? String
        
        var startDate = event["start_date"] as? NSDate
        cell?.detailTextLabel?.text = self.dateFormatter.stringFromDate(startDate!)
        
        return cell!
        
    }
    
    
    func getSortedEventKeys() -> [String] {
        var keys = self.events.keys.array.sorted { (date1String :String, date2String :String) -> Bool in
            
            var date1 = self.sectionDateFormatter.dateFromString(date1String)
            var date2 = self.sectionDateFormatter.dateFromString(date2String)
            
            if date1!.compare(date2!) == NSComparisonResult.OrderedDescending {
                return false
            }
            
            return true
        }
        
        return keys
    }
    
    
    func getEventsBySectionIndex(index :Int) -> [PFObject] {
        
        var keys = getSortedEventKeys()
        
        var key = keys[index]
        
        var eventsForKey = self.events[key]

        return eventsForKey!
        
    }
    
    func getEventByIndexPath(indexPath :NSIndexPath) -> PFObject {
       
        var eventsForSection = self.getEventsBySectionIndex(indexPath.section)
        var event = eventsForSection[indexPath.row]
        
        return event
    }
    
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        var keys = self.getSortedEventKeys()
        
        var keyName = keys[section]
        return keyName
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.events.keys.array.count
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.selectedEvent = self.getEventByIndexPath(indexPath)
        self.performSegueWithIdentifier("EditEventSegue", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "EditEventSegue") {
            var destController = segue.destinationViewController as AddEventViewController
            destController.editMode = true
            destController.event = selectedEvent
        }
    }
    
    
}
