//
//  PrayerRequestDetailViewController.swift
//  SmallGroup
//
//  Created by Austin Aldrich on 11/6/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

import Foundation

class PrayerRequestDetailViewController : UIViewController {
    
    var prayerRequest :PFObject?
    
    @IBOutlet var descriptionTextView :UITextView?
    
    override func viewDidLoad() {
        self.descriptionTextView?.text = prayerRequest!["description"] as String
        
    }


    
    
}