//
//  MyAccountViewController.swift
//  SmallGroup
//
//  Created by Austin Aldrich on 10/4/14.
//  Copyright (c) 2014 Missions in Motion. All rights reserved.
//

import UIKit

class MyAccountViewController: XLFormViewController {
    
    var accountForm = XLFormDescriptor()
    var section = XLFormSectionDescriptor()
    var saveSection = XLFormSectionDescriptor()
    var firstNameRow = XLFormRowDescriptor(tag: "first_name", rowType: XLFormRowDescriptorTypeText, title: "First Name")
    
    var lastNameRow = XLFormRowDescriptor(tag: "last_name", rowType: XLFormRowDescriptorTypeText, title: "Last Name")
    
    var saveButtonRow = XLFormRowDescriptor(tag: "save_button", rowType: XLFormRowDescriptorTypeButton, title: "Save")

    override func viewDidLoad() {
        super.viewDidLoad()

        setLoginLogoutButton()
        
        self.firstNameRow.required = true
        self.lastNameRow.required = true
        self.saveButtonRow.action.formSelector = "save"
        self.saveButtonRow.cellConfig.setObject(Branding.getSystemBlueColor(), forKey: "textLabel.textColor")
        
        section.addFormRow(firstNameRow)
        section.addFormRow(lastNameRow)
        
        saveSection.addFormRow(saveButtonRow)
        
        self.accountForm.addFormSection(section)
        self.accountForm.addFormSection(saveSection)
        
        self.form = accountForm
        
        // Do any additional setup after loading the view.
    }
    
    
    func save() {
        
       var validForm = self.doValidations()
        
        if(!validForm) { return }
        
        var user = PFUser.currentUser()!
        user["first_name"] = self.firstNameRow.value as String
        user["last_name"] = self.lastNameRow.value as String
        
        user.save()
        
        self.view.makeToast("Profile saved!", duration: 2.0, position: "center")
        
        var window = UIApplication.sharedApplication().delegate?.window!
        var storyboard = window!.rootViewController?.storyboard!
        window?.rootViewController = storyboard?.instantiateViewControllerWithIdentifier("Authenticated")! as? UIViewController
        
    }
    
    
    override func viewWillAppear(animated: Bool) {
        var user = PFUser.currentUser()
        
        if(user == nil) {
           self.redirectToLoginViewController()
        }
        else {
            self.firstNameRow.value = user["first_name"] as String?
            self.lastNameRow.value = user["last_name"] as String?
        }
        
    }
    
    func setLoginLogoutButton() {
        
        
        if(PFUser.currentUser() != nil) {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Sign out", style: UIBarButtonItemStyle.Done, target: self, action: "signOut")
        }
        
        else {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Sign in", style: UIBarButtonItemStyle.Done, target: self, action: "signIn")
            
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func signOut() {
        PFUser.logOut()
        self.redirectToLoginViewController()
        
    }
    
    func signIn() {
        self.setLoginLogoutButton()
      
    }
    
    func redirectToLoginViewController() {
        var login = LoginViewController()
        self.presentViewController(login, animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
