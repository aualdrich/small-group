module MemberHelper
	def day_selected(day, member)
		member.days.any? {|d| d == day}
	end

	def days_string(member)
		return "" if member.days.nil?

		member.days.join(", ")
	end


end
