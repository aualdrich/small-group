class MembersController < ApplicationController
	def index
		@members = Member.asc(:name)
	end

	def new
		@member = Member.new
		@member.days = []
		@days = get_days

	end

	def edit
		@member = Member.find(params[:id])
		@days = get_days
	end

	def create
		@member = Member.new(member_params)

		if @member.save
			redirect_to members_path
		else
			@days = get_days
			render :new
		end
	end

	def update
		@member = Member.find(params[:id])

		if @member.update_attributes(member_params)
			redirect_to members_path
		else
			@days = get_days
			render :edit
		end
	end

	def destroy
		@member = Member.find(params[:id])
		@member.destroy

		redirect_to members_path
	end

	def get_days
		["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]
	end

	def names_for_today
		today = Time.now.strftime("%a")

		names = Member.asc(:name).select {|n| n.days.any? {|d| d == today}}.map { |m| m.name }

		render json: names

	end

	def prayer_email
		@names = Member.all.pluck(:email)
		PrayerList.prayer_email(@names).deliver

		render :json => {:success => "true"}
	end

	def get_members
		@names = Member.asc(:name)

		render :json => {:names => @names}
	end


	private
		def member_params
			params.require(:member).permit(:name, :email, :phone, :days => [])

		end


end
