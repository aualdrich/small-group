class Member
	include Mongoid::Document
	field :name, type: String
	field :days, type: Array
	field :email, type: String
	field :phone, type: String
end
